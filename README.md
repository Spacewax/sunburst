# Sunburst

A blogging system written using the Symfony Framework starting with version 4.


## License

[GPLv3](LICENSE)

## Planned Features

* Multilingual (i18n) Content
* European Cookie Law Compliance
* Admin Interface
* User Control Interface

## How to get started

1. git clone https://gitlab.com/Spacewax/sunburst.git sunburst

2. Add Database credentials in the .env file

3. If you don't have a DB yet, do this:
  * php bin/console doctrine:database:create

4. composer update --with-dependencies


## Documentaion

Not Available yet.
